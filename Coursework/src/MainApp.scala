import MainApp.getMostRecentTempForCities

import scala.io.Source
import scala.io.StdIn.{readInt, readLine}


object MainApp extends App {

  val data = readFile("weatherdata14march.txt")

  def readFile(filename: String): Map[String, List[(Int, Int)]] = {
    val result = for (line <- Source.fromFile(filename).getLines(); array = line.split(",").map(_.trim)) yield Map(array.head -> array.tail.map(x => {
      val y = x.split(":")
      (y(0).toInt, y(1).toInt)
    }).toList)
    result.toList.flatten.toMap
  }

  val actionMap: Map[Int, () => Boolean] = Map[Int, () => Boolean](1 -> handleOne, 2 -> handleTwo, 3 -> handleThree, 4 -> handleFour, 5 -> handleFive, 6 -> handleSix)

  var opt = 0
  do {
    opt = readOption
  } while (menu(opt))

  def readOption: Int = {
    println(
      """|Please select one of the following:
         |  1 - Get the most recent temperature values for each city (last year's)
         |  2 - Get the difference between min and max temperatures for each year, for each city
         |  3 - Get the average difference between min and max temperatures for each city
         |  4 - Get the greatest difference in min and max temperatures for each year, for each city
         |  5 - Select a city to get last year's min and max temperatures, and average min and max temperatures
         |  6 - Quit""".stripMargin)
    readInt()
  }

  def menu(option: Int): Boolean = {
    actionMap.get(option) match {
      case Some(f) => f()
      case None =>
        println("Sorry, that command is not recognised")
        true
    }
  }

  def handleOne(): Boolean = {
    printMostRecentTempForCities(getMostRecentTempForCities)
    true
  }

  def handleTwo(): Boolean = {
    printMinMaxDifference(getMinMaxDifferenceForCityAndYear)
    true
  }

  def handleThree(): Boolean = {
    printAverageMinMaxTempForCity(getAverageMinMaxTempForCity(getMinMaxDifferenceForCityAndYear))
    true
  }

  def handleFour(): Boolean = {
    printGreatestDiffMinMaxTemps(getGreatestDiffMinMaxTemps(getMinMaxDifferenceForCityAndYear))
    true
  }

  def handleFive(): Boolean = {
    printTempForGivenCity(getTempForGivenCity)
    true
  }

  def handleSix(): Boolean = {
    println("Selected quit")
    false
  }

  def printMostRecentTempForCities(f: Map[String, (Int, Int)]): Unit = {
    for ((k, v) <- f) printf(s"$k: $v\n")
  }

  def printMinMaxDifference(f: Map[String, List[Int]]): Unit = {
    for ((k, v) <- f) printf(s"$k: $v\n")
  }

  def printAverageMinMaxTempForCity(f: Map[String, Int]): Unit = {
    for ((k, v) <- f) printf(s"$k: $v\n")
  }

  def printGreatestDiffMinMaxTemps(f: Map[String, Int]): Unit = {
    for ((k, v) <- f) printf(s"$k: $v\n")
  }

  def printTempForGivenCity(f: (String) => (String, List[(Int, Int)])): Unit = {
    val cities = readLine("Please enter cities to get temperatures for: ").split(" ").filter(data.contains(_)).map(_.trim).map(
      city => f(city)
    ).map {
      tuple => tuple._1 + "\n" + "Recent Temperatures (low, high):" + tuple._2.reverse.head + "\n" +
        "Average temperatures for city (low, high): " + (tuple._2.map(_._1).sum / tuple._2.size, tuple._2.map(_._2).sum / tuple._2.size) + "\n\n"
    }
    cities.foreach(line => println(line))
  }

  def getMostRecentTempForCities: Map[String, (Int, Int)] = {
    val recentTemps = for ((k, v) <- data) yield Map(k -> v.reverse.head)
    recentTemps.flatten.toMap
  }

  def getMinMaxDifferenceForCityAndYear: Map[String, List[Int]] = {
    val getMinMaxDifference = for ((k, v) <- data) yield Map(k -> v.map { case (a, b) => b - a })
    getMinMaxDifference.flatten.toMap
  }

  def getAverageMinMaxTempForCity(f: Map[String, List[Int]]): Map[String, Int] = {
    val getAverageMinMaxTemp = for ((k, v) <- f) yield Map(k -> (v.foldLeft(0)((a, b) => a + b) / v.size))
    getAverageMinMaxTemp.flatten.toMap
  }

  def getGreatestDiffMinMaxTemps(f: Map[String, List[Int]]): Map[String, Int] = {
    def findMax(xs: List[Int], max: Int): Int = xs match {
      case head :: tail => findMax(tail, if (head > max) head else max)
      case Nil => max
    }

    val getGreatestDiffMinMaxTemps = for ((k, v) <- f) yield Map(k -> findMax(v, v.size))
    getGreatestDiffMinMaxTemps.flatten.toMap
  }

  def getTempForGivenCity(city: String): (String, List[(Int, Int)]) = {
    data.get(city) match {
      case Some(x) => city -> x
    }
  }


}
